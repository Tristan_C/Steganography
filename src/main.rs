/* 
 * -----------------------------------------------------------------------------
 * File: main.rs
 * -----------------------------------------------------------------------------
 * Author(s): Tristan_C <tristanc97@gmail.com>
 * -----------------------------------------------------------------------------
 * This file provides the functionality for interacting with a user. 
 *
 * NOTE: This implementation currently assumes it is running on a little-endian
 * machine.  It is not likley to work on Big-endian machines.
 *
 * Exit codes:
 *  0 = success
 *  -1 = fail invalid args
 *  -2 = fail file error
 *  -3 = file too large
 *
 * -----------------------------------------------------------------------------
 */

extern crate libsteg;
use std::env;
use libsteg::Options;
use libsteg::FileTypes;
use libsteg::encode;
use libsteg::decode;

fn print_help(){
    let help_string = "Img_steg Version 1 written by Tristan\n\n\
    This Program hides and retrieves data from a BMP image file.\n\
    The encode option is set by default and does not need to be specified.\n\n\
    Encode Usage: <options> <image_name> <file_to_hide>\n\
    Decode Usage: <options> <image_to_decode>\n\n\
    Encode Example: \t Img_steg image.bmp file_to_hide.txt\n\
    Decode Example: \t Img_steg -d -v image.bmp -o decoded_file.txt\n\n\
    Options:\n\
    -h, --help    \t Display This help.\n\
    -d, --decode  \t Decode the specified image.\n\
    -o, --output  \t specify the file to output immediately after this option.\n\
    -e, --encode  \t Set the encode option. This option is set by default\n\
    -n, --nostore \t Dont store the size of the hidden file in the image.\n\
                  \t This means that you will need to save the size of the file\n\
                  \t manually in order to decode it.\n\
    -s, --size    \t Manually specify the size of the hidden file. Only applies\n\
                  \t to the decode option.\n\
    -v, --verbose \t Print detailed information about the operations being\n\
                  \t performed\n\
	-f, --filetype\t set the file type of the image.\n";
    
	println!("{}", help_string);
}

fn get_options(args: &[String]) -> Options {
    let mut options = Options::new();

    //for (mut i, arg) in args.iter().enumerate() {
    let mut iter = args.iter().enumerate();
    while let Some((i, mut arg)) = iter.next(){       
        if i == 0{
            continue;
        }

        match arg.as_ref(){
            "-h" | "--help" => {
                print_help();
                std::process::exit(0);
            }

            "-d" | "--decode" => {
                options.operation = 1;
            }

            "-o" | "--output" => {
                match iter.next() {
                    Some(next) => {
                        options.output = next.1.to_string();
                    }
                    None => {
                        println!("ERROR! Missing argument for output flag.");
                        std::process::exit(-1);
                    }
                }
            }

            "-e" | "--encode" => {
                options.operation = 0;
            }

            "-n" | "--nostore" => {
                options.nostore = true;
            }

            "-s" | "--size" => {
                //Some(( i, arg)) = iter.next();
                //iter.next();
                match iter.next() {
                    Some(next) => {
                        arg = next.1;
                    }
                    None => {
                        println!("ERROR! Missing argument for size flag.");
                        std::process::exit(-1);
                    }
                }

                match arg.parse::<u32>() {
                    Ok(n) => options.data_file_size = n,
                    Err(_e) => {
                        println!("ERROR! Invalid size parameter");
                        std::process::exit(-1);
                    }
                }
            }

            "-v" | "--verbose" => {
                options.verbose = true;
            }

            "-f" | "--filetype" => {
                match iter.next() {
                    Some(next) => {
                        arg = next.1;
                    }

                    None => {
                        println!("ERROR! Missing argument for filetype flag.");
                        std::process::exit(-1);
                    }
                }
                
                options.filetype = match arg.parse::<FileTypes>(){
                    Err(_e) => {
                        println!("ERROR! Incorrect filetype.");
                        std::process::exit(-1);
                    }

                    Ok(n) => n,
                };
            }

            _ => {
                if options.operation == 0 && options.image_name == "".to_string() && ! arg.starts_with("-"){
                    options.image_name = arg.to_string();
                }
                else {
                    if options.input_file == "".to_string() && ! arg.starts_with("-"){
                    options.input_file = arg.to_string();
                    }
                    else{
                        println!("ERROR! Invalid Argument '{}'", arg);
                        println!("Try using the -h flag for help.");
                        std::process::exit(-1);
                    }
                }

            }
        }
        
    }

    if options.input_file == "".to_string(){
        println!("Error! You must specify an input file!");
        std::process::exit(-1);
    }




    if options.operation == 0 && options.image_name == "".to_string(){
        println!("Error! You must specify an image file!");
        std::process::exit(-1);

    }    
    return options;

}


fn print_options(opt: &Options){
    if ! opt.verbose{
        return;
    }
    println!("options:");
    println!("operation = {}", opt.operation);
    println!("nostore = {}", opt.nostore);
    println!("size = {}", opt.data_file_size);
    println!("verbose = {}", opt.verbose);
    println!("image_name = {}", opt.image_name);
    println!("input_file = {}", opt.input_file);
    println!("output = {}", opt.output);
    //println!("filetype = {:?}", opt.filetype);
    // TODO Implement printing for enum FileTypes
    println!("");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut options = get_options(&args);
    print_options(&options);
    if options.operation == 0{
        encode(&mut options);
    }
    else{
        decode(&mut options);
    }
}

