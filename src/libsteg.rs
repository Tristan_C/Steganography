/* 
 * -----------------------------------------------------------------------------
 * File: libsteg.rs
 * -----------------------------------------------------------------------------
 * Author(s): Tristan_C <tristanc97@gmail.com>
 * -----------------------------------------------------------------------------
 * This file provides the underlying functionality to perform steganography
 * on images. 
 *
 * NOTE: This implementation currently assumes it is running on a little-endian
 * machine.  It is not likley to work on Big-endian machines.
 *
 * Exit codes:
 *  0 = success
 *  -1 = fail invalid args
 *  -2 = fail file error
 *  -3 = file too large
 *
 * -----------------------------------------------------------------------------
 */

use std::io;
use std::io::Read;
use std::io::prelude::*;
use std::fs::OpenOptions;
use std::fs::File;

pub struct Options{
	pub args: Vec<String>,
	pub operation: i32,
	pub nostore: bool,
	pub data_file_size: u32,
	pub verbose: bool,
    pub image_name: String,
	pub input_file: String,
	pub output: String,
	pub filetype: FileTypes,
}

impl Options{
    pub fn new() -> Options{
        return Options{ args: vec!["".to_string()],
                        operation: 0,
                        nostore: false,
                        data_file_size:0,
                        verbose: false,
                        image_name: "".to_string(),
                        input_file: "".to_string(),
                        output: "out".to_string(),
                        filetype: FileTypes::Bmp,
        }
    }
}


pub enum FileTypes{
    Bmp,
}

impl std::str::FromStr for FileTypes {
    type Err = ();

    fn from_str(s: &str) -> Result<FileTypes, ()> {
        match s {
            "bmp" => Ok(FileTypes::Bmp),
            _ => Err(()),
        }
    }
}

fn get_filesize(filename: String) -> io::Result<u64> {
    let file = File::open(filename)?;
    let metadata = file.metadata()?;
    return Ok(metadata.len());
}

// TODO pass by reference here.
fn get_bmp_start_index(filename: String) -> io::Result<u32>{
    let mut file = File::open(filename)?;
    let mut cur_byte: [u8; 1] = [0];
    let mut result: u32 = 0;

    // Pull out start offset from header.  (4 bytes starting at byte 10).
    for i in 10..=13 {
        file.seek(std::io::SeekFrom::Start(i))?;
        {
            let fref = std::io::Read::by_ref(&mut file);
            fref.take(5).read(&mut cur_byte)?;
        }
        if i != 10{
            result = (cur_byte[0] as u32) << 8 |result;
        }
        else{
            result = cur_byte[0].into();
        }
    }
    return Ok(result);
}


fn get_filetype(_filneame: String) -> FileTypes{
    // TODO Actually implement filetype checking here!
    return FileTypes::Bmp;
}

fn get_byte_spaceing_bmp(size_of_img: u64, 
                         img_start_ind: u32, 
                         size_of_data_file: u64) -> u64{

    let size_writable: u64 = size_of_img - img_start_ind as u64;
    return ((size_writable - 10) / 8) / size_of_data_file;
}

fn to_bool(n: u8) -> bool{
    return n != 0;
}


fn create_output_img_file_bmp(input_filesize: u64, opt: &mut Options) -> io::Result<u64>{
    // Copy entirety of image to output.
    std::fs::copy(opt.image_name.clone(), opt.output.clone())?;

    // Insert size of our file in the header if needed.
    if ! opt.nostore{
        // Open output.
        let mut out_f = OpenOptions::new().write(true).open(opt.output.clone())?;
        out_f.seek(std::io::SeekFrom::Start(6))?;

        out_f.write(&[((input_filesize) & 0xFF)  as u8])?;
        out_f.write(&[((input_filesize >> 8) & 0xFF)  as u8])?;
        out_f.write(&[((input_filesize >> 16) & 0xFF)  as u8])?;
        out_f.write(&[((input_filesize >> 24) & 0xFF)  as u8])?;
    }
    
    return Ok(0);
}


fn write_file_to_bmp(img_filename:String,
                     img_start_ind: u32, 
                    byte_spaceing: u64, 
                    input_filename: String,
                    input_filesize: u64,
                    output_filename: String) -> io::Result<u64>{
    // Open files
    let mut out_f = OpenOptions::new().write(true).open(output_filename)?;
    let mut in_f = OpenOptions::new().read(true).open(input_filename)?;
    let mut img_f = OpenOptions::new().read(true).open(img_filename)?;

    // Seek to start of img data.
    out_f.seek(std::io::SeekFrom::Start(img_start_ind as u64))?;
    img_f.seek(std::io::SeekFrom::Start(img_start_ind as u64))?;
    
    let mut img_file_ind: u64 = img_start_ind as u64;

    // Define buffers.
    let mut in_buf: [u8; 1] = [0];
    let mut out_buf: [u8; 1] = [0];
    let mut img_buf: [u8; 1] = [0];

    let mut data_file_ind: u64 = 0;
    let mut bitmask: u8;
    let mut bit_to_be_inserted: bool;
    let mut lsb_img: bool;

    while data_file_ind < input_filesize{
        // Update buffers.
        {
            let in_f_ref = std::io::Read::by_ref(&mut in_f);
            in_f_ref.take(1).read(&mut in_buf)?;
        }

        bitmask = 128;
        // Mask out every bit of current byte, starting with msb.
        while bitmask >= 1{
            {
                let img_f_ref = std::io::Read::by_ref(&mut img_f);
                img_f_ref.take(1).read(&mut img_buf)?;
            }
            bit_to_be_inserted = to_bool(in_buf[0] & bitmask);
            lsb_img = to_bool(img_buf[0] & 01);
            out_buf[0] = img_buf[0];

            if bit_to_be_inserted != lsb_img{
                // bit_to_be_inserted and lsb_img differ. flip lsb_img and write
                out_buf[0] ^= 1;
                out_f.write(&out_buf)?;
            }

            bitmask = bitmask / 2;
            img_file_ind += byte_spaceing;
            img_f.seek(std::io::SeekFrom::Start(img_file_ind))?;
            out_f.seek(std::io::SeekFrom::Start(img_file_ind))?;
        }

        data_file_ind += 1;
    }

    return Ok(data_file_ind);
}

fn read_file_from_bmp(data_filesize: u64,
                    byte_spaceing: u64, 
                    img_start_ind: u32,
                    input_filename: String,
                    _input_filesize: u64,
                    output_filename: String) -> io::Result<u64>{
    // Open files
    let mut out_f = File::create(output_filename)?;
    let mut in_f = OpenOptions::new().read(true).open(input_filename)?;

    // Define buffers.
    let mut in_buf: [u8; 1] = [0];
    let mut out_buf: [u8; 1] = [0];

    let mut bit_count = 0;
    let mut in_file_ind: u64 = img_start_ind as u64;
    let mut recovered = 0;


    // Seek to start of img data.
    in_f.seek(std::io::SeekFrom::Start(in_file_ind as u64))?;

    while recovered < data_filesize{
        {
            let in_f_ref = std::io::Read::by_ref(&mut in_f);
            in_f_ref.take(1).read(&mut in_buf)?;
        }
        // Mask out lsb and add it to output buffer.
        out_buf[0] |= (in_buf[0] & 01) << (7 - bit_count);
        bit_count += 1;

        // If we have a whole byte then write it.
        if bit_count == 8{
            out_f.write(&out_buf)?;
            recovered += 1;
            out_buf[0] = 0;
            bit_count = 0;
        }

        in_file_ind += byte_spaceing;
        in_f.seek(std::io::SeekFrom::Start(in_file_ind))?;

    }

    return Ok(recovered);
}

fn encode_bmp(opt: &mut Options){
    // Get input file size.
    let input_filesize = match get_filesize(opt.input_file.clone()) {
        Err(_e) => {
            println!("ERROR! File '{}' cannot be opened", opt.input_file);
            std::process::exit(-2);
        }
        Ok(n) => n,
    };

    // Get image file size.
    let image_filesize = match get_filesize(opt.image_name.clone()){
        Err(_e) => {
            println!("ERROR! file '{}' cannot be opened.", opt.image_name);
            std::process::exit(-2);
        }
        Ok(n) => n,
    };

    // Get start index from bmp 
    // TODO do proper error handling here. Not unwrap!
    let img_start_ind = match get_bmp_start_index(opt.image_name.clone()){
        Err(_e) => {
            println!("ERROR! file '{}' cannot be opened", opt.image_name);
            std::process::exit(-2);
        }

        Ok(n) => n,
    };

    let byte_spaceing = get_byte_spaceing_bmp(image_filesize, 
                                              img_start_ind, 
                                              input_filesize);

    if byte_spaceing < 1 {
        // File too large
        println!("ERROR! The file to hide is too large!");
        println!("File to hide size = {}", input_filesize);
        println!("Writable image size = {}", image_filesize);
        std::process::exit(-3);
    }

    // Create output file.
    match create_output_img_file_bmp(input_filesize, opt){
        Err(_e) => {
            println!("Error creating new image! check that you have proper file\
            permissions and try again.");
            std::process::exit(-2);
        }

        Ok(n) => n,
    };
    
    // Actually write the data to the output file.
    write_file_to_bmp(opt.image_name.clone(),
                      img_start_ind, 
                      byte_spaceing, 
                      opt.input_file.clone(),
                      input_filesize,
                      opt.output.clone()).unwrap();

    if opt.verbose{
        println!("start ind = {}", img_start_ind);
        println!("Byte spaceing = {}", byte_spaceing);
    }
}

fn get_data_filesize_bmp(input_filename: String) -> io::Result<u32>{
    let mut ret: u32 = 0;
    let mut in_buf: [u8; 1] = [0];
    let mut in_f = OpenOptions::new().read(true).open(input_filename)?;
    in_f.seek(std::io::SeekFrom::Start(6))?;
    let mut shift = 0;
    for _i in 0..4{
        {
            let in_f_ref = std::io::Read::by_ref(&mut in_f);
            in_f_ref.take(1).read(&mut in_buf)?;
        }
        ret = ret | (in_buf[0] as u32) << shift;
        shift += 8;
    }

    return Ok(ret);
}

fn decode_bmp(opt: &mut Options){
    // Calculate data file size if not specified.
    let data_filesize: u64;
    if opt.data_file_size == 0{
        data_filesize = match get_data_filesize_bmp(opt.input_file.clone()){
            Err(_e) =>{ 
                println!("Error acceessing file {}", opt.input_file.clone());
                std::process::exit(-2);
            }

            Ok(n) => n as u64,
        };
    }
    else{
        data_filesize = opt.data_file_size.clone() as u64;
    }
    let image_file_size = match get_filesize(opt.input_file.clone()) {
        Err(_e) => {
            println!("Error acceessing file {}", opt.input_file.clone());
            std::process::exit(-2);
        }

        Ok(n) => n as u64,
    };
    let start_ind = match get_bmp_start_index(opt.input_file.clone()){
        Err(_e) => {
            println!("Error acceessing file {}", opt.input_file.clone());
            std::process::exit(-2);
        }

        Ok(n) => n,
    };

    let byte_spaceing = get_byte_spaceing_bmp(image_file_size, 
                                              start_ind, 
                                              data_filesize);

    let recovered = match read_file_from_bmp(data_filesize,
                                         byte_spaceing, 
                                         start_ind,
                                         opt.input_file.clone(),
                                         data_filesize,
                                         opt.output.clone()){
        Err(_e) => {
            println!("Error accessing files provided. Check that you have write\
                     permissions and try again.");
            std::process::exit(-2);
        }

        Ok(n) => n,
    };

    if opt.verbose{
        println!("data filesize = {}", data_filesize);
        println!("start_ind = {}", start_ind);
        println!("Recovered {} bytes", recovered);
        println!("byte spaceing = {}", byte_spaceing);
    }
}

pub fn encode(opt: &mut Options){
    opt.filetype = get_filetype(opt.input_file.clone());
    match opt.filetype{
        FileTypes::Bmp => encode_bmp(opt),
    }
}

pub fn decode(opt : &mut Options){
    opt.filetype = get_filetype(opt.input_file.clone());
    match opt.filetype{
        FileTypes::Bmp => decode_bmp(opt),
    }
}

    
