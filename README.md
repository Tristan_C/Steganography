# Steganography

A simple command line utility to hide data in image data.  Written in rust. 
This application was tested under a gnu/linux based OS, but it *should* 
comiple and run under any Operating System supported by Rust. 
Note: this application assumes 
that the machine it is running on is little endian, and is unlikley to work
properly on big endian machines.  This program also currently only works with
bmp images.  Data is hidden in the image by placeing the bits of the data file
in the least significant bits of the image file, speading the altered bits out
as much as possible. 

# Compiling
* Rust and Cargo must first be installed. Find out more [here](https://www.rust-lang.org/tools/install).

* Next simply navigate to the project directory and compile/run the program with
the command 
`cargo run` be sure to seperate cargo arguments from program arguments with `--`
ex. `cargo run -- -v -e img.bmp input.txt` An explination of how to use the
program can be printed with the command `cargo run -- --help`
